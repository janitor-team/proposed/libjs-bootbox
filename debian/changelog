libjs-bootbox (5.5.3~ds-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on uglifyjs.

  [ Jonas Smedegaard ]
  * declare compliance with Debian Policy 4.6.1
  * update copyright info: update coverage
  * update git-buildpackage config:
    + use DEP-14 git branches
    + enable automatic DEP-14 branch name handling
    + add usage config

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 16 Aug 2022 21:03:18 +0200

libjs-bootbox (5.5.2~ds-2) unstable; urgency=medium

  * fix have autopkgtest depend on nodejs
  * update copyright info:
    + use Reference field (not License-Reference);
      tighten lintian overrides
    + update coverage
  * use debhelper compatibility level 13 (not 12)
  * declare compliance with Debian Policy 4.6.0
  * simplify source helper script copyright-check

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 23 Dec 2021 08:59:31 +0100

libjs-bootbox (5.5.2~ds-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Debian Janitor ]
  * use secure copyright file specification URI
  * set upstream metadata fields:
    Bug-Database Bug-Submit Repository Repository-Browse

  [ Jonas Smedegaard ]
  * use brotli compression suffix .brotli
    (not .br used for language breton)
  * declare compliance with Debian Policy 4.5.1

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 03 Dec 2020 20:50:15 +0100

libjs-bootbox (5.5.1~ds-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * copyright: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 29 Oct 2020 17:51:11 +0100

libjs-bootbox (5.4.0~ds-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * fix homepage URL to use insecure protocol
  * watch:
    + use dversionmangle=auto
    + use repacksuffix=~ds
      (not ~dfsg: stripped parts are only a nuisance, no violation)
  * use debhelper compatibility level 12 (not 10);
    build-depend on debhelper-compat (not debhelper)
  * declare compliance with Debian Policy 4.5.0
  * copyright: extend coverage
  * recommend libjs-bootstrap4 (favored over libjs-bootstrap)

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 19 Apr 2020 10:06:03 +0200

libjs-bootbox (5.3.2~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Declare compliance with Debian Policy 4.4.1.
  * Mark autopkgtest as superficial.
  * Update source path in rules.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 02 Oct 2019 17:38:41 +0200

libjs-bootbox (5.1.3~dfsg-2) unstable; urgency=medium

  * Release to unstable.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 07 Jul 2019 20:19:46 -0300

libjs-bootbox (5.1.3~dfsg-1) experimental; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Fix clean generated documentation files.
  * Update copyright info:
    + Exclude minified files from repackaged source.
    + Extend copyright for main upstream author.
  * Build also *.locales and *.all library files.
  * Update autopkgtest:
    + Depend on and use node-domino.
    + Test all built library files.
  * Extend patch 2001 to cover more privacy breach.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 05 Jul 2019 13:50:54 -0300

libjs-bootbox (4.4.0~dfsg-1) unstable; urgency=low

  * Initial release
    Closes: #867286.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 03 Feb 2019 11:14:03 +0100
