#!/usr/bin/make -f

STEMS = bootbox bootbox.all bootbox.locales
# generate documentation unless nodoc requested
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
DOCS = README.html README.txt
endif

override_dh_auto_build: $(patsubst %,debian/js/%.min.js.gz,$(STEMS)) \
 $(DOCS)

override_dh_installdocs:
	dh_installdocs --all -- $(DOCS)

# use staging dir for browser library (easier to install and clean)
debian/js/%.js: %.js
	mkdir --parents debian/js
	cp --force --target-directory debian/js $<

# optimize JavaScript for browser use
# * include source-map
debian/js/%.min.js: debian/js/%.js
	uglifyjs --compress --mangle \
		--source-map "base='$(abspath $(dir $@))',url='$(notdir $@).map'" \
		--output $@ \
		-- $<

# pre-compress for browser use
%.gz: %
	pigz --force --keep -11 -- $<
	brotli --force --keep --best --suffix=.brotli -- $<

%.html: %.md
	pandoc --from gfm-raw_html --to html --standalone --output $@ $<

%.txt: %.md
	pandoc --from gfm-raw_html --to plain --output $@ $<

%:
	dh $@

.SECONDARY:
